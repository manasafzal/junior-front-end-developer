
# Technical Test for Junior Front-End Developer

## Objective:

The purpose of this test is to evaluate your ability to use Playwright for end-to-end regression tests, as well as your proficiency in Vue 3 and Vuetify. 

The test is designed to be completed in around 2-3 hours, but there is no time limit and you may spend longer on the test if you want to. 

You are not required to use Typescript, but doing so will be viewed positively.

Please follow the instructions carefully and provide detailed responses where necessary.

## Prerequisites:

* Fork this project on gitlab and clone to your local device.
* Ensure you have Playwright installed and configured.
* Set up a new Vue 3 project with Vuetify.

## Task 1: Playwright End-to-End Test (60 minutes)

Create an end-to-end regression test for a simple Vue 3 application using Playwright.

### Requirements:

Write a Playwright script to perform the following actions:

* Open the Vue 3 application in a browser.
* Click on a button.
* Verify that a specific element appears on the page as a result of the button click.
* Take a screenshot of the page after the button click.

Ensure your test is structured, and comments are provided to explain the purpose of each step.

Include error handling mechanisms in case any step fails.

## Task 2: Vue 3 and Vuetify Component Implementation (30 minutes)

Add a new Vue component that utilises Vuetify components.

### Requirements:

Create a new Vue component named TaskList.vue.

Implement a simple task list using Vuetify components, with the following features:

* Display a list of tasks.
* Allow the user to add a new task.
* Allow the user to mark a task as completed.
* Use Vuetify styling appropriately.

Integrate the TaskList.vue component into the existing Vue 3 application.

## Task 3: Integration Testing (30 minutes)
Write integration tests to ensure the proper functionality of the TaskList.vue component.

### Requirements:

Write playwright tests to test the following scenarios:

* Adding a new task.
* Marking a task as completed.
* Ensure the list updates accordingly.

## Submission:

After you have finished your work, please push your entire project to gitlab, make the repository public and email the url of the repo to james.taylor@esprofiler.com

The deadline for submissions is Thursday 18th January 2023 at 5pm.

Provide clear instructions on how to run the Playwright test and the Vue application.

Edit the README file to include any additional information or improvements you would make if given more time.

### Additional Improvements (Optional):

If you have extra time, please consider the following improvements:

* Enhancements to the Playwright test script.
* Stylistic improvements to the Vue 3 application.
* Additional functionality or features for the task list
.
Note: Ensure your code is clean, well-organised, and properly documented. If you encounter any issues during the test, document them in your submission and provide your best attempt at a solution.
