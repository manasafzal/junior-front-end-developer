const { test, expect } = require('@playwright/test');
    
test.beforeEach(async ({ browserName }, testInfo) => {
    // Skip tests if the browser is WebKit
    test.skip(browserName === 'webkit', 'Skipping tests on WebKit due to playwright crashing on mac.');
  });
test('Vue 3 App button click test', async ({ page }) => {

    // URL of the Vue 3 application
    const appUrl = 'http://localhost:8080'; 

    // CSS selectors for the button and the element to verify
    const buttonSelector = '#myButton'; 
    const elementSelector = '#resultElement'; 

    // Open the Vue 3 application in a browser
    await page.goto(appUrl);

    await page.waitForLoadState('networkidle'); // Waits for no more network connections for at least 500 ms

    // Click on a button
    // error handling to ensure the button exists and is clickable
    try {
        await page.click(buttonSelector);
    } catch (error) {
        throw new Error(`Failed to click the button: ${error.message}`);
    }

    // Verify that a specific element appears on the page as a result of the button click
    // error handling if the element is not found
    try {
        await expect(page.locator(elementSelector)).toBeVisible();
    } catch (error) {
        throw new Error(`The expected element did not appear after the button click: ${error.message}`);
    }

    // Take a screenshot of the page after the button click
    // Handle any errors that might occur during screenshot capture
    try {
        await page.screenshot({ path: 'screenshot.png' });
    } catch (error) {
        throw new Error(`Failed to take a screenshot: ${error.message}`);
    }
});

test.describe('Task List Tests', () => {
    test.beforeEach(async ({ browserName }, testInfo) => {
        // Skip tests if the browser is WebKit
        test.skip(browserName === 'webkit', 'Skipping tests on WebKit due to playwright crashing on mac.');
      });

  test('should allow adding a new task', async ({ page }) => {
    // Go to the application URL
    await page.goto('http://localhost:8080'); 

    // Input a new task
    await page.getByLabel('New Task').fill('New Test Task');

    // Click the "Add Task" button
    await page.click('text=Add Task');

    // Verify the new task appears in the list
    const taskContent = await page.textContent('v-list-item-content');
    expect(taskContent).toContain('New Test Task');
  });

  test('should mark a task as completed', async ({ page }) => {
    // Assuming the task is already added from the previous test
    await page.goto('http://localhost:8080'); 

    // Add a new task first
    await page.getByLabel("New Task").fill('Task to Complete');
    await page.click('text=Add Task');

    // Locate the task by its text and click the "Done" button
    await page.click("text=Task to Complete >> ../.. >> text=Done");

    // Wait for the action to complete and the class to be applied
    await page.waitForSelector("v-list-item-content.task-completed:has-text('Task to Complete')");

    // Verification: Check if the task has 'task-completed' class
    const completedClass = await page.getAttribute("v-list-item-content:has-text('Task to Complete')", 'class');
    expect(completedClass).toContain('task-completed');
  });
});

