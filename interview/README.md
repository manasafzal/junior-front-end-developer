# My Attempt at this technical interview

First, clone this repo.

Then navigate to the interview folder.

Then use the following command to run the project: npm run serve

In order to run the tests, open a separate terminal window and use the following command: npx playwright test

During my attempt, playwright was crashing regularly whenever webkit was being tested. The only solution i could come up with was to ignore webkit testing as it had some issues on mac. Thank you for your understanding.